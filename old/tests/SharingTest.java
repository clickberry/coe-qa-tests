package coe.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.switchToWindow;

import java.io.File;
import java.io.IOException;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;



import com.codeborne.selenide.Screenshots;
import com.google.common.io.Files;

import static com.codeborne.selenide.Selenide.*;

/**
 * Test Case: Save Project
 */

public class SharingTest extends TestBase {	
		
/**
 * Share Project to Facebook
 */
	 	 
@Features("Sharing Project to social networks")	
@Stories("Share Project to Facebook")
@Title("Share Project to Facebook")
//@Test

public static void Test1_SharetoFB() {
	
    coe.pages.LoginPage.OpenLoginPage();
	
	coe.pages.LoginPage.EmailUserLoginSuccess(
			"123456@123.com", 
			"123456");
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.CheckUploadPage();
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.ChangeProjectName("Share to FB Project");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();	
	
	coe.pages.PlayerPage.PlayVideo();
	
	coe.pages.PlayerPage.AddTag();
	
	coe.pages.PlayerPage.AddComment("New test comment");
	
	coe.pages.PlayerPage.SaveTag();	
	
	coe.pages.SavePage.CheckScreenshot();
	
	coe.pages.SavePage.CreateComment("Test FB Share");
	
	coe.pages.SavePage.SaveProject();
	
	sleep(5000);
	
	coe.pages.SavePage.FBShare();
	
	switchToWindow("Facebook");
	
	coe.pages.FacebookPage.FBLoginWindow("qamary@mail.ru", "qa123456");
	
	switchToWindow("Post to Facebook");
	
	coe.pages.FacebookPage.FBCreatePost("Test FB Share");
	
	switchToWindow("Clickberry Online Editor");
	
	sleep(5000);
	
	open("https://www.facebook.com/");
	
	coe.pages.FacebookPage.FBCreatedPostCheck("Test FB Share");
	
	switchToWindow("Video on Clickberry Online Storage");
	
	coe.pages.PortalPages.CheckOtherUserProjectTitle("Share to FB Project");
	
	coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	coe.pages.PortalPages.Logout();
	
	open(baseUrl);	
	

}

/**
 * Share Project to Vkontakte	
 */

@Features("Sharing Project to social networks")	
@Stories("Share Project to Vkontakte")
@Title("Share Project to Vkontakte")	
//@Test

public static void Test2_SharetoVK() {
	
    coe.pages.LoginPage.OpenLoginPage();
	
	coe.pages.LoginPage.EmailUserLoginSuccess(
			"123456@123.com", 
			"123456");
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.CheckUploadPage();
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.ChangeProjectName("Share to VK Project");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();	
	
	coe.pages.PlayerPage.PlayVideo();
	
	coe.pages.PlayerPage.AddTag();
	
	coe.pages.PlayerPage.AddComment("New test comment");
	
	coe.pages.PlayerPage.SaveTag();	
	
	coe.pages.SavePage.CheckScreenshot();
	
	coe.pages.SavePage.CreateComment("Test VK Share");
	
	coe.pages.SavePage.SaveProject();
	
	sleep(5000);
	
	coe.pages.SavePage.VKShare();	
	
	sleep(2000);
	
	switchToWindow("VK | Login");
	
	coe.pages.VKPage.VKloginWindow("79175986240", "qa123456");
	
	sleep(2000);
	
	switchToWindow("ВКонтакте | Поделиться ссылкой");
	
	coe.pages.VKPage.VKCreatePost("Test VK Share");
	
	sleep(2000);
	
	switchToWindow("Clickberry Online Editor");
	
	open("http://vk.com");	
	
	coe.pages.VKPage.VKCreatedPostCheck("Test VK Share");
	
	switchToWindow("Video on Clickberry Online Storage");
	
	coe.pages.PortalPages.CheckOtherUserProjectTitle("Share to VK Project");
	
	coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	coe.pages.PortalPages.Logout();
	
	open(baseUrl);	
	

}


/**
 * Share Project to Twitter
 */

@Features("Sharing Project to social networks")	
@Stories("Share Project to Twitter")
@Title("Share Project to Twitter")
//@Test

public static void Test3_SharetoTwitter() {
	
    coe.pages.LoginPage.OpenLoginPage();
	
	coe.pages.LoginPage.EmailUserLoginSuccess(
			"123456@123.com", 
			"123456");
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.CheckUploadPage();
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.ChangeProjectName("Share to Twitter Project");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();	
	
	coe.pages.PlayerPage.PlayVideo();
	
	coe.pages.PlayerPage.AddTag();
	
	coe.pages.PlayerPage.AddComment("New test comment");
	
	coe.pages.PlayerPage.SaveTag();	
	
	coe.pages.SavePage.CheckScreenshot();
	
	coe.pages.SavePage.CreateComment("Test Twitter Share");
	
	coe.pages.SavePage.SaveProject();
	
	coe.pages.SavePage.TwitterShare();	
	
	switchToWindow("Login on Twitter");
	
	//open("https://twitter.com/");
	
	coe.pages.TwitterPage.TwitterLoginPage("qapaul@mail.ru", "qa123456");
	
	switchToWindow("Clickberry Online Editor");
	
    coe.pages.SavePage.TwitterShare();	
	
	switchToWindow("Post a Tweet on Twitter");	
	
	coe.pages.TwitterPage.TwitterCreatePost("Test Twitter Share");
	
	switchToWindow("Twitter");	
	
	coe.pages.TwitterPage.TwitterCreatedPostCheck("Test Twitter Share");
	
	switchToWindow("Video on Clickberry Online Storage");
	
	coe.pages.PortalPages.CheckOtherUserProjectTitle("Share to Twitter Project");
	
	coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	coe.pages.PortalPages.Logout();
	
	open(baseUrl);	
}

	
/**
 * Share Project to Google
 */

@Features("Sharing Project to social networks")	
@Stories("Share Project to Google")
@Title("Share Project to Google")
//@Test

	public static void Test4_SharetoGoogle() {
		
	    coe.pages.LoginPage.OpenLoginPage();
		
		coe.pages.LoginPage.EmailUserLoginSuccess(
				"123456@123.com", 
				"123456");
		
		coe.pages.UploadPage.UploadVideo();
		
		coe.pages.UploadPage.CheckUploadPage();
		
		coe.pages.UploadPage.SelectVideoFile();
		
		coe.pages.UploadPage.ChangeProjectName("Share to Google Project");
		
		coe.pages.UploadPage.UploadFilePass();
		
		coe.pages.PlayerPage.CheckPlayerPage();	
		
		coe.pages.PlayerPage.PlayVideo();
		
		coe.pages.PlayerPage.AddTag();
		
		coe.pages.PlayerPage.AddComment("New test comment");
		
		coe.pages.PlayerPage.SaveTag();	
		
		coe.pages.SavePage.CheckScreenshot();
		
		coe.pages.SavePage.CreateComment("Test Google Share");
		
		coe.pages.SavePage.SaveProject();
		
		sleep(5000);
		
		coe.pages.SavePage.GoogleShare();	
		
		sleep(2000);
		
		switchToWindow("Google+");
		
		coe.pages.GooglePage.GoogleLogin("cat4qa@gmail.com", "qa123456789");
		
		switchToWindow("Share on Google+");
		
		coe.pages.GooglePage.GoogleCreatePost();
		
		sleep(2000);
		
		switchToWindow("Clickberry Online Editor");
		
		open("https://plus.google.com");		
		
		coe.pages.GooglePage.GoogleCreatedPostCheck();
		
		switchToWindow("Video on Clickberry Online Storage");
		
		coe.pages.PortalPages.CheckOtherUserProjectTitle("Share to Google Project");
		
		coe.pages.PortalPages.CloseProject();
		
		coe.pages.PortalPages.DeleteProject();
		
		coe.pages.PortalPages.Logout();
		
		open(baseUrl);	
		
	}	
	
	
/**
 *Share Project to Pinterest
 */

@Features("Sharing Project to social networks")	
@Stories("Share Project to Pinterest")
@Title("Share Project to Pinterest")
//@Test

	public static void Test5_SharetoPinterest() {
		
	    coe.pages.LoginPage.OpenLoginPage();
		
		coe.pages.LoginPage.EmailUserLoginSuccess(
				"123456@123.com", 
				"123456");
		
		coe.pages.UploadPage.UploadVideo();
		
		coe.pages.UploadPage.CheckUploadPage();
		
		coe.pages.UploadPage.SelectVideoFile();
		
		coe.pages.UploadPage.ChangeProjectName("Share to Pinterest Project");
		
		coe.pages.UploadPage.UploadFilePass();
		
		coe.pages.PlayerPage.CheckPlayerPage();	
		
		coe.pages.PlayerPage.PlayVideo();
		
		coe.pages.PlayerPage.AddTag();
		
		coe.pages.PlayerPage.AddComment("New test comment");
		
		coe.pages.PlayerPage.SaveTag();	
		
		coe.pages.SavePage.CheckScreenshot();
		
		coe.pages.SavePage.CreateComment("Test Pinterest Share");
		
		coe.pages.SavePage.SaveProject();
		
		sleep(5000);
		
		coe.pages.SavePage.PinterestShare();	
		
		sleep(2000);
		
		switchToWindow("Pinterest: Discover and save creative ideas");
		
		coe.pages.PinterestPage.PinterestLogin("qapaul@mail.ru", "qa123456");
		
		switchToWindow("Pinterest: Discover and save creative ideas");
		
		coe.pages.PinterestPage.PinterestCreatePost("Test Pinterest Share");
		
		sleep(2000);
		
		switchToWindow("Clickberry Online Editor");
		
		open("https://www.pinterest.com/");	
		
		coe.pages.PinterestPage.PinterestCreatedPostCheck("Test Pinterest Share");
		
		switchToWindow("Video on Clickberry Online Storage");
		
		coe.pages.PortalPages.CheckOtherUserProjectTitle("Share to Pinterest Project");
		
		coe.pages.PortalPages.CloseProject();
		
		coe.pages.PortalPages.DeleteProject();
		
		coe.pages.PortalPages.Logout();
		
		open(baseUrl);	
		
	}	
	
/**
 * Share project to LinkedIn	
 */
	
//@Test

	public static void Test6_SharetoLinkedIn() {
		
	    coe.pages.LoginPage.OpenLoginPage();
		
		coe.pages.LoginPage.EmailUserLoginSuccess(
				"123456@123.com", 
				"123456");
		
		coe.pages.UploadPage.CheckUploadPage();
		
		coe.pages.UploadPage.SelectVideoFile();
		
		coe.pages.UploadPage.ChangeProjectName("Share to LinkedIn Project");
		
		coe.pages.UploadPage.UploadFilePass();
		
		coe.pages.PlayerPage.CheckPlayerPage();	
		
		coe.pages.PlayerPage.PlayVideo();
		
		coe.pages.PlayerPage.AddTag();
		
		coe.pages.PlayerPage.AddComment("New test comment");
		
		coe.pages.PlayerPage.SaveTag();	
		
		coe.pages.SavePage.CheckScreenshot();
		
		coe.pages.SavePage.CreateComment("Test LinkedIn Share");
		
		coe.pages.SavePage.SaveProject();
		
		sleep(5000);
		
		coe.pages.SavePage.LinkedInShare();	
		
		sleep(2000);
		
		switchToWindow("����� | LinkedIn");
		
		coe.pages.LinkedInPage.LinkedInLogin("qapaul@mail.ru", "qa123456");
		
		sleep(2000);
		
		
		
		switchToWindow("��������� | ���������� �������");
		
		coe.pages.VKPage.VKCreatePost("Test VK Share");
		
		sleep(2000);
		
		switchToWindow("Clickberry Online Editor");
		
		open("http://vk.com");	
		
		coe.pages.VKPage.VKCreatedPostCheck("Test VK Share");
		
		switchToWindow("Video on Clickberry Online Storage");
		
		coe.pages.PortalPages.CheckOtherUserProjectTitle("Share to VK Project");
		
		coe.pages.PortalPages.CloseProject();
		
		coe.pages.PortalPages.DeleteProject();
		
		coe.pages.PortalPages.Logout();
		
		open(baseUrl);	
		

	}
	
	@Attachment(type = "image/png")
	public byte[] screenshot() throws IOException {
	  File screenshot = Screenshots.getScreenShotAsFile();
	  return Files.toByteArray(screenshot);
	}
	
	@AfterMethod

    public static void LogOut() {
	
	close();
       	 	       
}
	
}