package coe.tests;

import java.io.File;
import java.io.IOException;

import org.junit.Rule;
import org.testng.annotations.*;

import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.junit.ScreenShooter;
import com.google.common.io.Files;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.*;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

/**
 * Test Case: Upload Video from File
 * @author User
 *
 */

public class UploadPageTest extends TestBase {		 
	
/**
 * 	Upload Video from File - High Quality
 */
@Features("Upload Video File")
@Stories("Select video quality for video upload")
@Title("Upload Video File in High quality")
//@Test

public static void Test1_UploadFromFileHigh() {
	
	coe.pages.LoginPage.OpenLoginPage();
	
	coe.pages.LoginPage.EmailUserLoginSuccess(
			"new-valid@email.com", 
			"123456");
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.CheckUploadPage();
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.SelectQualityHigh();
	
	coe.pages.UploadPage.ChangeProjectName("High Quality");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();	
	
	coe.pages.PlayerPage.SaveTag();	
	
    coe.pages.SavePage.SaveProject();
	
	sleep(5000);
	
	open("http://qa.clickberry.tv/");
	
	coe.pages.PortalPages.OpenMySnapsPage();
	
	coe.pages.PortalPages.OpenProject();
	
	coe.pages.PortalPages.CheckPersonalProjectTitle("High Quality");
	
	coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	coe.pages.PortalPages.Logout();
	
	open(baseUrl);	
	
}

/**
 * 	Upload Video from File - Low Quality
 */
@Features("Upload Video File")
@Stories("Select video quality for video upload")
@Title("Upload Video File in Low quality")
//@Test

public static void Test2_UploadFromFileLow() {
	
	coe.pages.LoginPage.OpenLoginPage();
	
	coe.pages.LoginPage.EmailUserLoginSuccess(
			"new-valid@email.com", 
			"123456");
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.CheckUploadPage();
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.SelectQualityHigh();
	
	coe.pages.UploadPage.SelectQualityLow();
	
	coe.pages.UploadPage.ChangeProjectName("Low Quality");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();	
	
    coe.pages.PlayerPage.SaveTag();	
	
    coe.pages.SavePage.SaveProject();
	
	sleep(5000);
	
    open("http://qa.clickberry.tv/");
	
	coe.pages.PortalPages.OpenMySnapsPage();
	
    coe.pages.PortalPages.OpenProject();
	
	coe.pages.PortalPages.CheckPersonalProjectTitle("Low Quality");
	
	coe.pages.PortalPages.CloseProject();
		
	coe.pages.PortalPages.DeleteProject();
	
	coe.pages.PortalPages.Logout();
	
	open(baseUrl);	
	
}

/**
 * Change Project Name - negative tests.
 */
@Features("Select Project Name")
@Stories("Select Incorrect Project Name")
@Title("Select Incorrect Project Name")
//@Test

public static void Test3_SetProjectNameFail() {
	
    coe.pages.LoginPage.OpenLoginPage();
	
	coe.pages.LoginPage.EmailUserLoginSuccess(
			"new-valid@email.com", 
			"123456");
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.CheckUploadPage();
	
	// Project with empty name
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.ChangeProjectName("");
	
	coe.pages.UploadPage.UploadFileFail();
	
	// Project name containing only white spaces
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.ChangeProjectName(" ");
		
	coe.pages.UploadPage.UploadFileFail();
	
	// Project with too long name	
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.ChangeProjectName("Neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeewwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwprrrrrrrrrrrrrrrrrrrrrrrrrrrooooooooooooooooooooooooooooooooooooooooooooooojjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeecttttttttttttttttttttttttttt");

	coe.pages.UploadPage.UploadFileFail();
	
	refresh();
	
	coe.pages.UploadPage.Logout();
}


/**
 * Change Project Name - positive tests.
 */
@Features("Select Project Name")
@Stories("Select Correct Project Name")
@Title("Select Correct Project Name")
//@Test

public static void Test4_SetProjectNamePass() {
	
    coe.pages.LoginPage.OpenLoginPage();
	
	coe.pages.LoginPage.EmailUserLoginSuccess(
			"new-valid@email.com", 
			"123456");
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.CheckUploadPage();
	
	coe.pages.UploadPage.SelectVideoFile();
	
	// Project with long name
	
	coe.pages.UploadPage.ChangeProjectName("Neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeewwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwprrrrrrrrrrrrrrrrrrrrrrrrrrroooooooooooooooooooooooooooooooooooooooooooooojjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeecttttttttttttttttttttttttttt");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();
	
    coe.pages.PlayerPage.SaveTag();	
	
    coe.pages.SavePage.SaveProject();
	
	sleep(5000);
	
	open("http://qa.clickberry.tv/");
	
	coe.pages.PortalPages.OpenMySnapsPage();
	
	coe.pages.PortalPages.OpenProject();
		
    coe.pages.PortalPages.CheckPersonalProjectTitle("Neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeewwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwprrrrrrrrrrrrrrrrrrrrrrrrrrroooooooooooooooooooooooooooooooooooooooooooooojjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeecttttttttttttttttttttttttttt");
		
    coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	open(baseUrl);	
	
	// Project Name with numbers
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.SelectVideoFile();
	
    coe.pages.UploadPage.ChangeProjectName("Project #1234567890");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();
	
	coe.pages.PlayerPage.SaveTag();	
		
	coe.pages.SavePage.SaveProject();
		
    sleep(5000);
	
	open("http://qa.clickberry.tv/");
	
	coe.pages.PortalPages.OpenMySnapsPage();
	
	coe.pages.PortalPages.OpenProject();
	
    coe.pages.PortalPages.CheckPersonalProjectTitle("Project #1234567890");
    
    coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	open(baseUrl);	
	
    // Project Name with symbols
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.SelectVideoFile();
	
    coe.pages.UploadPage.ChangeProjectName("~!@#$%^&*()_+={}[]|/?.,><№;:*");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();
	
	coe.pages.PlayerPage.SaveTag();	
	
	coe.pages.SavePage.SaveProject();
		
    sleep(5000);
	
    open("http://qa.clickberry.tv/");
	
	coe.pages.PortalPages.OpenMySnapsPage();
	
    coe.pages.PortalPages.OpenProject();
	
    coe.pages.PortalPages.CheckPersonalProjectTitle("~!@#$%^&*()_+={}[]|/?.,><№;:*");
    
    coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	open(baseUrl);
	
    // Project Name with symbols
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.SelectVideoFile();
	
    coe.pages.UploadPage.ChangeProjectName("Новый проект");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();
	
    coe.pages.PlayerPage.SaveTag();	
	
	coe.pages.SavePage.SaveProject();
		
    sleep(5000);
	
    open("http://qa.clickberry.tv/");
	
	coe.pages.PortalPages.OpenMySnapsPage();
	
    coe.pages.PortalPages.OpenProject();
	
    coe.pages.PortalPages.CheckPersonalProjectTitle("Новый проект");
    
    coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	open(baseUrl);
	
    // Project Name with symbols
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.SelectVideoFile();
	
    coe.pages.UploadPage.ChangeProjectName("مشروع جديدनयाँ परियोजना新项目úéáćžä");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();	
	
    coe.pages.PlayerPage.SaveTag();	
	
	coe.pages.SavePage.SaveProject();
		
    sleep(5000);
	
    open("http://qa.clickberry.tv/");
	
	coe.pages.PortalPages.OpenMySnapsPage();
	
    coe.pages.PortalPages.OpenProject();
	
    coe.pages.PortalPages.CheckPersonalProjectTitle("مشروع جديدनयाँ परियोजना新项目úéáćžä");
    
    coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	open(baseUrl);
	
	coe.pages.UploadPage.Logout();
	
}

@Attachment(type = "image/png")
public byte[] screenshot() throws IOException {
  File screenshot = Screenshots.getScreenShotAsFile();
  return Files.toByteArray(screenshot);
}


}
