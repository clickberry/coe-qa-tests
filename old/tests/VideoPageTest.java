package coe.tests;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;

import java.io.File;
import java.io.IOException;

import org.junit.Rule;
import org.testng.annotations.*;

import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.junit.ScreenShooter;
import com.google.common.io.Files;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

/**
 * Test Case: Create Tag
 */

public class VideoPageTest extends TestBase {	
	
/**
 * Test Player: play video, change sound, etc.
 */
@Features("Online Video Editor")
@Stories("Play uploaded video in Online Video Editor player")
@Title("Play uploaded video in Online Video Editor player")
//@Test

public static void Test1_PlayVideo() {
	
    coe.pages.LoginPage.OpenLoginPage();
	
	coe.pages.LoginPage.EmailUserLoginSuccess(
			"new-valid@email.com", 
			"123456");
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.CheckUploadPage();
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.ChangeProjectName("New Project 1");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();	
	
	coe.pages.PlayerPage.PlayVideo();
	
	coe.pages.PlayerPage.PauseVideo();
	
	coe.pages.PlayerPage.VideoSound();
	
    coe.pages.PlayerPage.SaveTag();	
	
	coe.pages.SavePage.SaveProject();
	
	sleep(5000);
	
	open("http://qa.clickberry.tv/");
	
	coe.pages.PortalPages.OpenMySnapsPage();
	
	coe.pages.PortalPages.OpenProject();
		
	coe.pages.PortalPages.CheckPersonalProjectTitle("New Project 1");
	    
	coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	coe.pages.PortalPages.Logout();
	
	open(baseUrl);	
	
}

/**
 * Create Tag
 */
@Features("Online Video Editor")
@Stories("Create new Snap in Online Video Editor")
@Title("Create new Snap in Online Video Editor with Comment widget")
//@Test

public static void Test2_CreateTag() {
	
    coe.pages.LoginPage.OpenLoginPage();
	
	coe.pages.LoginPage.EmailUserLoginSuccess(
			"new-valid@email.com", 
			"123456");
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.CheckUploadPage();
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.ChangeProjectName("New Project 2");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();	
	
	coe.pages.PlayerPage.PlayVideo();
	
	coe.pages.PlayerPage.AddTag();
	
	coe.pages.PlayerPage.AddComment("New test video");
	
	coe.pages.PlayerPage.SaveTag();
	
    coe.pages.SavePage.SaveProject();
	
    sleep(5000);
	
	open("http://qa.clickberry.tv/");
	
	coe.pages.PortalPages.OpenMySnapsPage();
	
	coe.pages.PortalPages.OpenProject();
		
	coe.pages.PortalPages.CheckPersonalProjectTitle("New Project 2");
	    
	coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	coe.pages.PortalPages.Logout();
	
	open(baseUrl);			
	
}

@Attachment(type = "image/png")
public byte[] screenshot() throws IOException {
  File screenshot = Screenshots.getScreenShotAsFile();
  return Files.toByteArray(screenshot);
}

}
