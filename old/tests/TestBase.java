package coe.tests;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

import ru.yandex.qatools.allure.annotations.Attachment;
import coe.util.PropertyLoader;
import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;

import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.testng.ScreenShooter;
import com.google.common.io.Files;

@Listeners({ ScreenShooter.class})

/**
 * Base class with configuration settings.
 *
 */

public abstract class TestBase { 	

  /**
   *  Start WebDriver with configuration from POM profile
   */

  @BeforeTest    
  
  public static void setUp() { 	  
	  
	  baseUrl = PropertyLoader.loadProperty("site.url");
	  
	  browser = PropertyLoader.loadProperty("browser.name");
			  
	  open(baseUrl);		 
	    
  }   
   
  /**
   * Quit WebDriver after all tests execution.
   */
  
  @AfterClass   
  
  public static void close() {  	  
	    closeWebDriver();	    
	  }   
 
  
}