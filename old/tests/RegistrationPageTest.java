package coe.tests;

import java.io.File;
import java.io.IOException;

import org.junit.Rule;
import org.testng.annotations.*;

import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.junit.ScreenShooter;
import com.google.common.io.Files;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

/**
 * Test Case: Application Registration Page.
 */

 public class RegistrationPageTest extends TestBase {	 
	
/**
 * Test Email Account registration - negative tests.
 */
@Features("New user registration")
@Stories("Register user with incorrect data")
@Title("Register user with incorrect data")
//@Test
		
	public void RegistrationNegativeTest() {
	
	// Open Registration Page
	
	coe.pages.LoginPage.OpenLoginPage();		
	coe.pages.LoginPage.OpenRegistrationPage();
	
	// User Name: null
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"", 
			"Valid@Email.com", 
			"Valid@Email.com", 
			"123456", 
			"123456");	
		
	// User Name > 65 symbols
	
	coe.pages.RegistrationPage.UserSignUpFail(
			"Neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeewwwwwwwwwwwwwwwwwwwwwww", 
			"Valid@Email.com", 
			"Valid@Email.com", 
			"123456", 
			"123456");	
		
	// User Email: null
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"", 
			"", 
			"123456", 
			"123456");	
		
	// User Email: 1 symbol
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"a", 
			"a", 
			"123456", 
			"123456");	
		
	// User Email: 321 symbols 
	/* COE-48
	coe.pages.RegistrationPage.UserSignUpFail(
			"New Test User", 
			"maaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaail@teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.ru", 
			"maaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaail@teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.ru", 
			"123456", 
			"123456");	
	*/	
	// User Email: no local part
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"mail.ru", 
			"mail.ru", 
			"123456", 
			"123456");	
		
	// User Email: local part > 64
	
	coe.pages.RegistrationPage.UserSignUpFail(
			"New Test User", 
			"maaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaail@mail.ru", 
			"maaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaail@mail.ru", 
			"123456", 
			"123456");	
		
	// User Email: local part starts with "."
	
	coe.pages.RegistrationPage.UserSignUpFail(
			"New Test User", 
			".newuser@mail.ru", 
			".newuser@mail.ru", 
			"123456", 
			"123456");	
		
	// User Email: no @
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"newuser.mail.ru", 
			"newuser.mail.ru", 
			"123456", 
			"123456");	
		
	// User Email: contains ".."
	
	coe.pages.RegistrationPage.UserSignUpFail(
			"New Test User", 
			"new..user@mail.ru", 
			"new..user@mail.ru", 
			"123456", 
			"123456");	
		
	// User Email: no domain part
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"newuser", 
			"newuser", 
			"123456", 
			"123456");	
		
	// User Email: domain part > 255
	/* COE-48
	coe.pages.RegistrationPage.UserSignUpFail(
			"New Test User", 
			"maaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaail@teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.ru", 
			"maaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaail@teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.ru", 
			"123456", 
			"123456");	
		
	// User Email: > 63 between "."
	
	coe.pages.RegistrationPage.UserSignUpFail(
			"New Test User", 
			"mail@teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.ru", 
			"mail@teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.ru", 
			"123456", 
			"123456");
		*/
	// User Email: domain part starts with "." 
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"newuser@.mail.ru", 
			"newuser@.mail.ru", 
			"123456", 
			"123456");
		
	// User Email: existing email
	
	coe.pages.RegistrationPage.UserSignUpFail(
			"New Test User", 
			"Valid@Email.com", 
			"Valid@Email.com", 
			"123456", 
			"123456");
		
	// User Email: invalid symbols
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"~!@#$%^&*()_+@Email.com", 
			"~!@#$%^&*()_+@Email.com", 
			"123456", 
			"123456");
		
	// User Email: invalid alphabet
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"����@�����.��", 
			"����@�����.��", 
			"123456", 
			"123456");
		
	// Email: valid@new_email.com
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"valid@new_email.com", 
			"valid@new_email.com", 
			"123456", 
			"123456");
		
	// Confirm Email: null
	
	// Confirm Email: not correct
	
	// Password: null
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"Valid@Email.com", 
			"Valid@Email.com", 
			"", 
			"");
		
	// Password: 5 symbols
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"Valid@Email.com", 
			"Valid@Email.com", 
			"12345", 
			"12345");
		
	// Password: > 100 symbols
	coe.pages.RegistrationPage.UserSignUpFail(
			"New Test User", 
			"Valid@Email.com", 
			"Valid@Email.com", 
			"11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111", 
			"11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
		
	// Confirm Password: null
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"Valid@Email.com", 
			"Valid@Email.com", 
			"123456", 
			"");
		
	// Confirm Password: incorrect value
	
	coe.pages.RegistrationPage.UserSignUpIncorrect(
			"New Test User", 
			"Valid@Email.com", 
			"Valid@Email.com", 
			"123456", 
			"223456");
	
	}

@Attachment(type = "image/png")
public byte[] screenshot() throws IOException {
  File screenshot = Screenshots.getScreenShotAsFile();
  return Files.toByteArray(screenshot);
}

/**
 * Return to Login Page
 */

@AfterTest

    public static void GoBack() {
	
	coe.pages.RegistrationPage.GoToLoginPage();
       	 	       
}
	
	

}
