package coe.tests;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.open;

import java.io.File;
import java.io.IOException;

import org.junit.Rule;
import org.testng.annotations.*;

import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.junit.ScreenShooter;
import com.google.common.io.Files;

import static com.codeborne.selenide.Selenide.*;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

/**
 * Test Case: Save Project
 */

public class SavePageTest extends TestBase {	
	
	
/**
 * Save Hidden Project	
 */
@Features("Save created Project on Portal")
@Stories("Select Project Privacy")
@Title("Save project as 'Hidden'")
//@Test

public static void Test1_SaveHiddenProject() {
	
    coe.pages.LoginPage.OpenLoginPage();
	
	coe.pages.LoginPage.EmailUserLoginSuccess(
			"123456@123.com", 
			"123456");
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.CheckUploadPage();
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.ChangeProjectName("Hidden Project");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();	
	
	coe.pages.PlayerPage.PlayVideo();
	
	coe.pages.PlayerPage.AddTag();
	
	coe.pages.PlayerPage.AddComment("New test comment");
	
	coe.pages.PlayerPage.SaveTag();	
	
	coe.pages.SavePage.CheckScreenshot();
	
	coe.pages.SavePage.SetHidden();
	
	coe.pages.SavePage.SaveProject();
	
	sleep(5000);
	
	open("http://qa.clickberry.tv/");
	
	coe.pages.PortalPages.OpenProject();
	
	coe.pages.PortalPages.CheckHiddenProject();
	
	coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	coe.pages.PortalPages.Logout();
	
	open(baseUrl);	
	

}

/**
 * Save Public Project	
 */
@Features("Save created Project on Portal")
@Stories("Select Project Privacy")
@Title("Save project as 'Public'")	
//@Test

public static void Test2_SavePublicProject() {
	
    coe.pages.LoginPage.OpenLoginPage();
	
	coe.pages.LoginPage.EmailUserLoginSuccess(
			"123456@123.com", 
			"123456");
	
	coe.pages.UploadPage.UploadVideo();
	
	coe.pages.UploadPage.CheckUploadPage();
	
	coe.pages.UploadPage.SelectVideoFile();
	
	coe.pages.UploadPage.ChangeProjectName("Public Project");
	
	coe.pages.UploadPage.UploadFilePass();
	
	coe.pages.PlayerPage.CheckPlayerPage();	
	
	coe.pages.PlayerPage.PlayVideo();
	
	coe.pages.PlayerPage.AddTag();
	
	coe.pages.PlayerPage.AddComment("New test comment");
	
	coe.pages.PlayerPage.SaveTag();	
	
	coe.pages.SavePage.CheckScreenshot();
	
	coe.pages.SavePage.SaveProject();
	
	sleep(5000);
	
	open("http://qa.clickberry.tv/");
	
	coe.pages.PortalPages.OpenProject();
	
	coe.pages.PortalPages.CheckPublicProject();
	
	coe.pages.PortalPages.CloseProject();
	
	coe.pages.PortalPages.DeleteProject();
	
	coe.pages.PortalPages.Logout();
	
	open(baseUrl);		

}

@Attachment(type = "image/png")
public byte[] screenshot() throws IOException {
  File screenshot = Screenshots.getScreenShotAsFile();
  return Files.toByteArray(screenshot);
}

}
