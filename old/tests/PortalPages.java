package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import ru.yandex.qatools.allure.annotations.Step;

import org.openqa.selenium.Alert;

import org.openqa.selenium.By;

public class PortalPages {
	
	private static SelenideElement MySnapsTab = $("a[href $= '/videos']");
	private static SelenideElement ProjectsList = $("div.list-tags.ng-scope");
	private static ElementsCollection Project = $$(By.xpath("//div[@ng-model='watch']/a"));
	
	private static SelenideElement UserProjectTitle = $(By.xpath("//div[@class='user-title']//span[@class='ng-binding']"));
	private static SelenideElement OtherUserProjectTitle = $(By.xpath("//div[@class='user-title']/h2"));
	
	private static SelenideElement ProjectHidden = $(By.xpath("//a[@ng-click='togglePublic(watch)']//i[@class='fa fa-key']"));
	private static SelenideElement ProjectPublic = $(By.xpath("//a[@ng-click='togglePublic(watch)']//i[@class='fa fa-globe']"));
	
	private static SelenideElement ProjectCloseButton = $(By.xpath("//div[@class='popup popup-close ng-scope']"));
	private static SelenideElement ProjectDeleteButton = $(By.xpath("//li[@class='trash']/a"));
	
	private static SelenideElement UserProfile = $("div.login");
	private static SelenideElement SignOutButton = $("a.btn.btn-login");
	
	private static SelenideElement PortalTabs = $("div.menu-tabs");
	private static SelenideElement SignInButton = $("span.icon-signin");
	
/**
 * Open My Snaps Page
 */
	
	@Step("Open My Snaps Page")
	public static void OpenMySnapsPage() {
		
		MySnapsTab.click();	
				
	}
	
/**
 * Open created Project	
 */
	@Step("Open created Project")
	public static void OpenProject() {
		
		MySnapsTab.click();
		ProjectsList.waitUntil(visible, 10000);
		Project.get(0).waitUntil(present, 5000).click();
				
	}

/**
  * Check Hidden Project	
  */
	@Step("Check Project status is 'Hidden'")	
	public static void CheckHiddenProject() {
			
		ProjectHidden.should(visible);	
			
	}

/**
  * Check Public Project	
  */
	@Step("Check Project status is 'Public'")		
	public static void CheckPublicProject() {
				
		ProjectPublic.should(visible);	
				
	}	
	
/**
 * Check Project Title	
 */
	@Step("Check Project Title")
	public static void CheckOtherUserProjectTitle(String ProjectName) {
		
		refresh();
		OtherUserProjectTitle.shouldHave(text(ProjectName));
		
	}
	
/**
  * Check Personal Project Title	
  */
	@Step("Check Personal Project Title")
    public static void CheckPersonalProjectTitle(String ProjectName) {
			
		sleep(2000);
		UserProjectTitle.shouldHave(text(ProjectName));
			
	}
	
/**
 * Close project	
 */
	@Step("Close project")
	public static void CloseProject() {
		
		ProjectCloseButton.click();
		
	}
	
/**
 * Delete Project
 */
	@Step("Delete created Project")
	public static void DeleteProject() {			
		
			ProjectDeleteButton.click();        
			Alert alert = switchTo().alert();
			alert.accept();		
		    sleep(2000);
	}
	
/**
 * Sign Out from Portal	
 */
	@Step("Sign Out from Portal")
	public static void Logout() {
		
		UserProfile.waitUntil(visible, 5000).hover();
		SignOutButton.waitUntil(visible, 10000).click();    
	     
		PortalTabs.waitUntil(visible, 5000);
		SignInButton.should(exist);   
		
	}
	
}
