package coe.tests;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.browser;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.switchToWindow;

import java.io.File;
import java.io.IOException;

import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.testng.ScreenShooter;

import org.junit.Rule;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.google.common.io.Files;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;
import coe.util.PropertyLoader;
import static com.codeborne.selenide.Selenide.*;

//@Listeners({ ScreenShooter.class})

/**
 * Test Case: Application Login Page.
 * 
 *
 */

public class LoginPageTest extends TestBase {		 
	
/**
 * Test Login via Email Account - negative tests.
 */
@Features("Login to Clickberry Online Editor")
@Stories("Login as Email user")
@Title("Login as Email user with incorrect credentials")
//@Test
	
   public void Test1_EmailUserLoginFail() {
	
   coe.pages.LoginPage.OpenLoginPage();
	
   coe.pages.LoginPage.EmailLoginForm();
   
   // User Email: null
   
   coe.pages.LoginPage.EmailUserLoginFail("", "123456");
   coe.pages.LoginPage.AssertFailMessage();
   
   // User Email: 1 symbol
   
   coe.pages.LoginPage.EmailUserLoginFail("a", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: 321 symbols
   
   coe.pages.LoginPage.EmailUserLoginFail("maaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaail@teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.ru", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: invalid symbols
   
   coe.pages.LoginPage.EmailUserLoginFail("~!@#$%^&*()_+@Email.com", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: incorrect alphabet
   
   coe.pages.LoginPage.EmailUserLoginFail("новая@почта.рф", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: no local part (mail.ru)
   
   coe.pages.LoginPage.EmailUserLoginFail("mail.ru", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: local part > 64 
   
   coe.pages.LoginPage.EmailUserLoginFail("maaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaail@mail.ru", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: local part starts with "." 
   
   coe.pages.LoginPage.EmailUserLoginFail(".newuser@mail.ru", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: no @ 
   
   coe.pages.LoginPage.EmailUserLoginFail("newuser.mail.ru", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: contains ".." 
   
   coe.pages.LoginPage.EmailUserLoginFail("new..user@mail.ru", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: no domain part 
   
   coe.pages.LoginPage.EmailUserLoginFail("newuser", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: domain part > 255
   
   coe.pages.LoginPage.EmailUserLoginFail("maaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaail@teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.ru", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: > 63 between "."
   
   coe.pages.LoginPage.EmailUserLoginFail("mail@teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeest.ru", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Email: domain part starts with "." 
   
   coe.pages.LoginPage.EmailUserLoginFail("newuser@.mail.ru", "123456");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Password: null
   
   coe.pages.LoginPage.EmailUserLoginFail("Valid@Email.com", "");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Password:12345
   
   coe.pages.LoginPage.EmailUserLoginFail("Valid@Email.com", "12345");
   coe.pages.LoginPage.AssertWrongEmail();
   
   // User Password:> 100 symbols
   
   coe.pages.LoginPage.EmailUserLoginFail("Valid@Email.com", "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
   coe.pages.LoginPage.AssertWrongEmail();
			
	}

/**
 * Test Login via Email Account - positive tests.
 */
@Features("Login to Clickberry Online Editor")
@Stories("Login as Email user")
@Title("Login as Email user with correct credentials")
@Test

   public void Test2_EmailUserLoginPass() {
	
	// Short email
	
	coe.pages.LoginPage.EmailUserLoginSuccess("a@aa.ru", "123456");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	// User Email: long value
	
	coe.pages.LoginPage.EmailUserLoginSuccess("wwwwwwwwwwwwwwwwwwuseeeeeeeeeerr@mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmaaaaaaaaaaaaaaaaaaaaaaaaa.com", "123456");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	// User Email with numbers
	
	coe.pages.LoginPage.EmailUserLoginSuccess("123456@123.com", "123456");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	coe.pages.LoginPage.EmailUserLoginSuccess("1valid@email.com", "123456");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	coe.pages.LoginPage.EmailUserLoginSuccess("valid@1email.com", "123456");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	// User Email with dots 
	
	coe.pages.LoginPage.EmailUserLoginSuccess("new.new.valid@email.net.com", "123456");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	// Email with hyphen
	
	coe.pages.LoginPage.EmailUserLoginSuccess("new-valid@email.com", "123456");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	coe.pages.LoginPage.EmailUserLoginSuccess("valid@new-email.com", "123456");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	// Email with symbols
	
	coe.pages.LoginPage.EmailUserLoginSuccess("~!^&$*-_=+{}|/@mail.ru", "123456");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	// User Password:100 symbols
	
	coe.pages.LoginPage.EmailUserLoginSuccess("Valid1@mail.ru", "1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();

	// Password with different alphabet & symbols
	
	//coe.pages.LoginPage.EmailUserLoginSuccess("valid12@email.com", "جديدngườiनए新用");
	//coe.pages.UploadPage.CheckUploadPage();
	//coe.pages.UploadPage.Logout();
	
	coe.pages.LoginPage.EmailUserLoginSuccess("Valid3@mail.ru", "!#$%&`*+/=?^`{|}~");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	//coe.pages.LoginPage.EmailUserLoginSuccess("Valid4@mail.ru", "New_Password_123456~!@#$%^&+_09新用戶");
	//coe.pages.UploadPage.CheckUploadPage();
	//coe.pages.UploadPage.Logout(); 
			
}


/**
 * Test Login via Social Networks	
 */
@Features("Login to Clickberry Online Editor")
@Stories("Login via Social Network")
@Title("Login as VK/FB/Twitter user")
@Test

public void Test3_SNUserLoginTestPass() {
	
	// Test Login via Twitter	
	coe.pages.LoginPage.COELoginTwitter();		
	switchToWindow("Twitter / Authorize an application");	
	coe.pages.TwitterPage.TwitterLoginWindow("qapaul@mail.ru", "qa123456");	
	switchToWindow("Clickberry Online Editor");	
	coe.pages.UploadPage.CheckUploadPage();	
	coe.pages.UploadPage.Logout();
	
	// Test Login via VK	
	coe.pages.LoginPage.COEloginVK();
	switchToWindow("VK | Login");
	coe.pages.VKPage.VKloginWindow("79250965893", "click4qa");
	switchToWindow("Clickberry Online Editor");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	// Test Login via FB
	coe.pages.LoginPage.COELoginFB();
	sleep(5000);
	switchToWindow("Facebook");
	coe.pages.FacebookPage.FBLogin("qajack@mail.ru", "qa123456");
	switchToWindow("Clickberry Online Editor");
	coe.pages.UploadPage.CheckUploadPage();
	coe.pages.UploadPage.Logout();
	
	
}

@Attachment(type = "image/png")
public byte[] screenshot() throws IOException {
  File screenshot = Screenshots.getScreenShotAsFile();
  return Files.toByteArray(screenshot);
}

}
