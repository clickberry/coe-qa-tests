package web.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

import ru.yandex.qatools.allure.annotations.Step;

import org.openqa.selenium.By;

public class LinkedInPage {
	
	private static SelenideElement linkedInEmailField = $(By.id("session_key-login"));
	private static SelenideElement linkedInPasswordField = $(By.id("session_password-login"));
	private static SelenideElement linkedInEnterButton = $(By.id("btn-primary"));
	
	
/**
 * Login to LinkedIn.
 * @param Email
 * @param Password
 */
	
	@Step("Login to LinkedIn")
	public static void linkedInLogin(String Email, String Password) {
		
		linkedInEmailField.sendKeys(Email);
		linkedInPasswordField.sendKeys(Password);
		linkedInEnterButton.click();	
		
	}

}
