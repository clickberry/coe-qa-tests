package web.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import ru.yandex.qatools.allure.annotations.Step;

import org.openqa.selenium.By;

public class PinterestPage {
	
	private static SelenideElement pinterestEmailField = $(By.id("userEmail"));
	private static SelenideElement pinterestPasswordField = $(By.id("userPassword"));
	private static SelenideElement pinterestSignUpButton = $(By.xpath("//button[@class='Button Module btn hasText medium primary large continueButton rounded']"));
	
	//private static SelenideElement pinterestCreatePostScreenshot = $(By.xpath("//div[@class='Module PinBookmarklet modalStyle']//div[@class='pinContainer']//div[@class='pinHolder']//img"));
	//private static SelenideElement pinterestCreatePostName = $(By.xpath("//div[@class='Module PinBookmarklet modalStyle']//div[@class='pinContainer']//div[@class='pinMeta ']/p"));
	private static ElementsCollection pinterestBoard = $$(By.xpath("//div[@class='BoardLabel Module pinCreate']"));
	private static SelenideElement pinterestPinItButton = $(By.xpath("//div[@class='BoardLabel Module pinCreate']/button"));
	
	private static SelenideElement pinterestUserLink = $(By.xpath("//button[@class='Module UserNavigateButton merged']"));
	private static SelenideElement pinterestBoardLink = $(By.xpath("//div[@class='boardName']"));
	
	private static ElementsCollection pinterestPostLink = $$(By.xpath("//div[@class='Image Module pinUiImage']"));
	private static SelenideElement pinterestCreatedPostScreenshot = $(By.xpath("//div[@class='imageContainer']//img"));
	private static SelenideElement pinterestCreatedPostComment = $(By.xpath("//div[@class='pinDesc nonCanonicalDesc']"));
	private static SelenideElement pinterestCreatedPostLink = $(By.xpath("//a[@class='paddedPinLink']"));
	
/**
 * Login to Pinterest.	
 * @param Email
 * @param Password
 */
	
	@Step("Login to Pinterest")
	public static void pinterestLogin(String Email, String Password) {
		
		pinterestEmailField.sendKeys(Email);
		pinterestPasswordField.sendKeys(Password);
		pinterestSignUpButton.click();
				
	}
	
/**
 * Create new post in Pinterest.	
 * @param ProjectComment
 */
	
	@Step("Create new post in Pinterest")
	public static void pinterestCreatePost(String ProjectComment) {
		
		//PinterestCreatePostScreenshot.shouldBe(visible);
		//PinterestCreatePostName.shouldHave(text(ProjectComment));
		pinterestBoard.get(0).hover();
		pinterestPinItButton.click();
		
	}
	
/**
 * Check created post in Pinterest.	
 * @param ProjectComment
 */
	
	@Step("Check created post in Pinterest")
	public static void checkPinterestCreatedPost(String ProjectComment) {
		
		pinterestUserLink.click();
		pinterestBoardLink.click();
		pinterestPostLink.get(0).click();
		pinterestCreatedPostComment.shouldHave(text(ProjectComment));
		pinterestCreatedPostScreenshot.shouldBe(visible);
		pinterestCreatedPostLink.click();		
		
	}

}
