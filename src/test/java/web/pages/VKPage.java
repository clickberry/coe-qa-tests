package web.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import ru.yandex.qatools.allure.annotations.Step;

import org.openqa.selenium.By;


public class VKPage {
	private static SelenideElement vkLoginForm = $(By.id("login_submit"));
	
	private static SelenideElement vkEmailFieldWindow = $(By.name("email"));
	private static SelenideElement vkPasswordFieldWindow = $(By.name("pass"));
	private static SelenideElement vkEnterButtonWindow = $(By.xpath("//div[@id='box']//button[@id='install_allow']"));
	
	private static SelenideElement vkCreatePostScreenshot = $(By.xpath("//div[@class='share_labeled']//img"));
	private static SelenideElement vkCreatePostTitle = $(By.id("share_title"));
	private static SelenideElement vkShareButton = $(By.id("post_button"));
		
	private static ElementsCollection vkCreatedPostName = $$(By.xpath("//div[@class='post_info']//div[@class='wall_post_text']"));
	private static ElementsCollection vkCreatedPostLink = $$(By.xpath("//div[@class='post_info']//div[@class='page_media_link_desc_wrap ']/a"));
	
/**
 * Open VK Login window.
 */
	@Step ("Open VK Login window")		
	public static void vkLoginOpen() {
		
		sleep(5000);
		vkLoginForm.should(exist);
		
	}

/**
  * Login to Vkontakte in the opened window.
  * @param Phone
  * @param Password
  */
		
	@Step("Login to Vkontakte in the opened window")
	public static void vkloginWindow(String Phone, String Password) {
		
		vkEmailFieldWindow.sendKeys(Phone);
		vkPasswordFieldWindow.sendKeys(Password);
		sleep(2000);
		vkEnterButtonWindow.click();
			
	}
	
/**
 * Create new post in Vkontakte.
 * @param ProjectComment
 */
	
	@Step("Create new post in Vkontakte")
	public static void vkCreatePost(String ProjectComment) {
		
		sleep(2000);
		vkCreatePostScreenshot.shouldBe(visible);
		vkCreatePostTitle.shouldHave(text(ProjectComment));
		vkShareButton.click();
	}
	
/**
 * Check created post in Vkontakte.	
 * @param ProjectComment
 */
	
	@Step("Check created post in Vkontakte")
	public static void checkVKCreatedPostk(String ProjectComment) {
		
		sleep(5000);
		refresh();
		vkCreatedPostName.get(0).shouldHave(text(ProjectComment));
		vkCreatedPostLink.get(0).shouldHave(text(ProjectComment));
		vkCreatedPostLink.get(0).click();
	}	
}
