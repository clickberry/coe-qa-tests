package web.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import ru.yandex.qatools.allure.annotations.Step;

import org.openqa.selenium.JavascriptExecutor;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

import org.openqa.selenium.By;

public class FacebookPage {	
	
	private static SelenideElement fbLoginForm = $(By.xpath("//div[@class='login_form_container']"));
	private static SelenideElement fbEmailField = $(By.id("email"));
	private static SelenideElement fbPasswordField = $(By.id("pass"));
	private static SelenideElement fbLoginButton = $(By.xpath("//div[@class='login_form_container']//button[@id='loginbutton']"));
		
	private static SelenideElement fbCreatePostScreenshot = $(By.xpath("//div[@class='clearfix shareRedesignContainer']/a/img"));
	private static SelenideElement fbCreatePostTitle = $(By.xpath("//div[@class='attachmentText fsm fwn fcg']/span"));
	private static SelenideElement fbShareButton = $(By.id("u_0_4"));
	
	private static SelenideElement fbCreatedPostName = $(By.xpath("//div[@id='substream_0']//div[@class='_6lz _6mb ellipsis']"));
	private static SelenideElement fbCreatePostLink = $(By.xpath("//div[@id='substream_0']//div[@class='_6ks']/a"));
	
/**
 * 	Check FB Login Form.
 */
	
	@Step("Check FB Login Form")
	public static void checkFBForm() {		
		
		sleep(5000);
		fbLoginForm.should(exist);
		
	}
	
/**
 * Login to Facebook in the opened window
 * @param Email
 * @param Password
 */
	
	@Step("Login to Facebook in the opened window")
	public static void fbLogin(String Email, String Password) {	
		
		fbEmailField.sendKeys(Email);
		sleep(5000);
		fbPasswordField.sendKeys(Password);
		sleep(5000);
		JavascriptExecutor jse;
		jse = (JavascriptExecutor)getWebDriver();
		jse.executeScript("document.getElementById('loginbutton').click()");
		
		
		//fbLoginButton.click();
		sleep(5000);
		
	}
	
/**
 * Create new Post in Facebook.
 * @param ProjectComment
 */
	
	@Step("Create new Post in Facebook")
	public static void createFBPost(String ProjectComment) {
		
		sleep(2000);
		fbCreatePostScreenshot.shouldBe(visible);
		fbCreatePostTitle.shouldHave(text(ProjectComment));
		fbShareButton.click();
		
	}
	
/**
 * Check created post in Facebook.
 * @param ProjectComment
 */
	
	@Step("Check created post in Facebook")	
	public static void checkFBCreatedPost(String ProjectComment) {		
		
		sleep(2000);
		fbCreatedPostName.shouldHave(text(ProjectComment));
		fbCreatePostLink.click();
				
	}		
	
}
