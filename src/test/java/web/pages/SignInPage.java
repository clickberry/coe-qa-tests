package web.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

import com.codeborne.selenide.SelenideElement;



/**
 * 
 * Base class with user logins & logouts.
 *
 */

public class SignInPage { 
	
	private static SelenideElement FacebookButton = $(By.xpath("//button[@ng-click='goFacebook()']"));
	private static SelenideElement VkontakteButton = $(By.xpath("//button[@ng-click='goVk()']"));
	private static SelenideElement GoogleButton = $(By.xpath("//button[@ng-click='goGoogle()']"));
	private static SelenideElement TwitterButton = $(By.xpath("//button[@ng-click='goTwitter()']"));
	
	private static SelenideElement EmailField = $(By.name("signinEmail"));
	private static SelenideElement PasswordField = $(By.name("signinPassword"));
	private static SelenideElement SubmitButton =  $(By.xpath("//button[@type='submit']"));
	private static SelenideElement CancelButton = $(By.xpath("//div[@class='layout-row']//a[@ui-sref='home']"));
	
	private static SelenideElement LoginFailDialog = $(By.xpath("//md-dialog"));
	private static SelenideElement LoginFailDialogButton = $(By.xpath("//md-dialog//button"));
	

/**
 * Check Sign In page
 */

public static void CheckSignIn() {
	
	EmailField.should(exist);
	PasswordField.should(exist);
	SubmitButton.should(exist);
}
	
/**
 * Click Facebook button.
 */
	
public static void FbButtonClick() {  
	    FacebookButton.click();  	    
 }	

/**
 * Click Vkontakte button.
 */

public static void VkontakteButtonClick() { 	    
	    VkontakteButton.click();	   	      
 }

/**
 * Click Google button.
 */

public static void GoogleButtonClick() { 	
	    GoogleButton.click();             
 }

/**
 * Click Twitter button.
 */

public static void TwitterButtonClick() {  
	   TwitterButton.click();   
 }

/**
 * Login to Portal as Email user.
 */

public static void EmailUserLogin(String Email, String Password) {  	  
	   EmailField.sendKeys(Email);
	   PasswordField.sendKeys(Password);
	   sleep(2000);
	   SubmitButton.click();                  
 }

/**
 * Login form is marked as incorrectly filled
 */

public static void LoginFail() {
	
	LoginFailDialog.shouldBe(visible);
	LoginFailDialogButton.click();
	
}

/**
 * Cancel login
 */

public static void LoginCancel() {
	CancelButton.click();
}

}
