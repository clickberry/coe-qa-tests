package web.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import ru.yandex.qatools.allure.annotations.Step;

import org.openqa.selenium.By;

public class TwitterPage {
	
	private static SelenideElement twitterLoginForm = $(By.id("oauth_form"));
	
	private static SelenideElement twitterEmailFieldWindow = $(By.id("username_or_email"));
	private static SelenideElement twitterPasswordFieldWindow = $(By.id("password"));
	private static SelenideElement twitterAuthorizeButtonWindow = $(By.id("allow"));
	
	private static SelenideElement twitterEmailFieldPage = $(By.xpath("//input[@class='js-username-field email-input js-initial-focus']"));
	private static SelenideElement twitterPasswordFieldPage = $(By.xpath("//input[@class='js-password-field']"));
	private static SelenideElement twitterEnterButtonPage = $(By.xpath("//button[@class='submit btn primary-btn']"));	
	
	private static SelenideElement twitterCreatePost = $(By.id("status"));
	private static SelenideElement twitterTweetButton = $(By.xpath("//input[@class='button selected submit']"));
	
	private static ElementsCollection twitterCreatedPost = $$(By.xpath("//div[@class='content']/p"));
	private static ElementsCollection twitterCreatedPostLink = $$(By.xpath("//div[@class='content']/p/a"));	
	
/**
 * 	Open Twitter Login window.
 */
	
	@Step("Open Twitter Login window")		
	public static void openTwitterLogin() {
			
		twitterLoginForm.waitUntil(visible, 5000);
		
	}
	
/**
 * Login to Twitter in the opened window.
 * @param Email
 * @param Password
 */
	
	@Step("Login to Twitter in the opened window")
	public static void twitterLogin(String Email, String Password) {
		
		twitterPasswordFieldWindow.sendKeys(Password);
		twitterEmailFieldWindow.sendKeys(Email);
		twitterAuthorizeButtonWindow.click();	
		sleep(2000);
		
	}
	
/**
 * Login to Twitter on the opened page.	
 * @param Email
 * @param Password
 */
	
	@Step("Login to Twitter on the opened page")
	public static void twitterLoginPage(String Email, String Password) {
		
		twitterPasswordFieldPage.sendKeys(Password);
		twitterEmailFieldPage.sendKeys(Email);
		twitterEnterButtonPage.click();	
		sleep(2000);
		
	}
	
/**
 * Create new post in Twitter.	
 * @param ProjectComment
 */
	
	@Step("Create new post in Twitter")
	public static void twitterCreatePost(String ProjectComment) {
		
		twitterCreatePost.shouldHave(text(ProjectComment));
		twitterTweetButton.click();
		
	}
	
/**
 * Check created post in Twitter.
 * @param ProjectComment
 */
	
	@Step("Check created post in Twitter")
	public static void checkTwitterCreatedPost(String ProjectComment) {
		
		twitterCreatedPost.get(0).shouldHave(text(ProjectComment));
		twitterCreatedPostLink.get(0).click();
	}	

}
