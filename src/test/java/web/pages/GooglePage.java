package web.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.text;

import ru.yandex.qatools.allure.annotations.Step;

import org.openqa.selenium.By;

public class GooglePage {
	
	private static SelenideElement googleEmailField= $(By.id("Email"));	
	private static SelenideElement googlePasswordField = $(By.id("Passwd"));	
	private static SelenideElement googleNextButton = $(By.id("next"));	
	private static SelenideElement googleSignInButton = $(By.id("signIn"));
	
	private static SelenideElement googleCreatePostLink = $(By.xpath("//div[@class='YPIndd']/a"));
	private static SelenideElement googleShareButton = $(By.xpath("//div[@guidedhelpid='sharebutton']"));
	
	private static ElementsCollection googleCreatedPostLink = $$(By.xpath("//a[contains(@href, 'qa.clickberry.tv')]"));
	
	
/**
 * Login to Google.
 * @param Email
 * @param Password
 */
	
	@Step("Login to Google")
	public static void googleLogin(String Email, String Password) {
		
		googleEmailField.sendKeys(Email);
		googleNextButton.click();
        googlePasswordField.sendKeys(Password);
        googleSignInButton.click();        
		
	}
	
/**
 * 	Create new post in Google.
 */
	
	@Step("Create new post in Google")
	public static void googleCreatePost() {
		
		googleCreatePostLink.shouldHave(text("qa.clickberry.tv"));
		googleShareButton.click();
	}
	
/**
 * 	Check created post in Google.
 */
	
	@Step("Check created post in Google")
	public static void checkGoogleCreatedPost() {
		
		sleep(5000);
		googleCreatedPostLink.get(0).click();
	}
	
}
