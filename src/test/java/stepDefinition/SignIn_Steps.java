package stepDefinition;

import cucumber.api.java.en.*;

public class SignIn_Steps {
	
	@Given("^User is on Editor Login Page$")
	public void user_is_on_Editor_Page() throws Throwable {
	    pages.IndexPage.openLoginPage();
	    pages.IndexPage.closeScreencast();
	}
	
	@Given("^User enters autouser@mail\\.ru Email credentials$")
	public void user_enters_autouser_mail_ru_Email_credentials() throws Throwable {
	    pages.IndexPage.emailUserLogin("autouser@mail.ru", "qa123456");
	}

	@Then("^User successfully logins$")
	public void user_successfully_logins() throws Throwable {
	    pages.IndexPage.checkSuccessMessage();
	}

	@Given("^User enters newusertest@mail\\.com Email credentials$")
	public void user_enters_newusertest_mail_com_Email_credentials() throws Throwable {
		pages.IndexPage.emailUserLogin("newusertest@mail.com", "qa123456");
	}

	@Given("^User enters EmailUserTest@mail\\.com Email credentials$")
	public void user_enters_EmailUserTest_mail_com_Email_credentials() throws Throwable {
	    pages.IndexPage.emailUserLogin("EmailUserTest@mail.com", "qa123456");
	}


	@Then("^My Projects page is displayed$")
	public void my_Projects_page_is_displayed() throws Throwable {
	    pages.MyProjectsPage.checkMyProjects();
	}

	@Then("^User logouts from editor$")
	public void user_logouts_from_editor() throws Throwable {
	    pages.IndexPage.logout();
	    pages.IndexPage.checkSocialLoginButtons();
	    pages.IndexPage.checkEmailLoginForm();
	}
	
	@Given("^User clicks FB login icon$")
	public void user_clicks_FB_login_icon() throws Throwable {
	    pages.IndexPage.loginFB();
	}

	@Given("^FB window is opened$")
	public void fb_window_is_opened() throws Throwable {
	    web.pages.FacebookPage.checkFBForm();
	}

	@Given("^User enters FB credentials$")
	public void user_enters_FB_credentials() throws Throwable {
	    web.pages.FacebookPage.fbLogin("qajack@mail.ru", "qa123456");
	}
	
	@Given("^User clicks VK login icon$")
	public void user_clicks_VK_login_icon() throws Throwable {
	    pages.IndexPage.loginVK();
	}

	@Given("^VK window is opened$")
	public void vk_window_is_opened() throws Throwable {
	    web.pages.VKPage.vkLoginOpen();
	}

	@Given("^User enters VK credentials$")
	public void user_enters_VK_credentials() throws Throwable {
		web.pages.VKPage.vkloginWindow("79175986240", "qa123456");
	}
	
	@Given("^User clicks Twitter login icon$")
	public void user_clicks_Twitter_login_icon() throws Throwable {
	    pages.IndexPage.loginTwitter();
	}

	@Given("^Twitter window is opened$")
	public void twitter_window_is_opened() throws Throwable {
	    web.pages.TwitterPage.openTwitterLogin();
	}

	@Given("^User enters Twitter credentials$")
	public void user_enters_Twitter_credentials() throws Throwable {
	    web.pages.TwitterPage.twitterLogin("qapaul@mail.ru", "qa123456");
	}	

}
