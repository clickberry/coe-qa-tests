package stepDefinition;

import cucumber.api.java.en.*;

public class VideoUpload_Steps {

	@Given("^User enters Email credentials$")
	public void user_enters_Email_credentials() throws Throwable {
	    pages.IndexPage.emailUserLogin("autouser@mail.ru", "qa123456");
	}

	@Given("^User opens Upload Video page$")
	public void user_opens_Upload_Video_page() throws Throwable {
	    pages.UploadPage.openUploadVideoPage();
	    pages.UploadPage.checkUploadVideoPage();
	}

	@Then("^User selects video to upload$")
	public void user_selects_video_to_upload() throws Throwable {
	    pages.UploadPage.selectVideoFile();
	}	

	@Then("^Video is uploading$")
	public void video_is_uploading() throws Throwable {
		pages.UploadPage.uploadFilePass();
	}

	@Then("^Player page is opened$")
	public void player_page_is_opened() throws Throwable {
		pages.PlayerPage.checkPlayerPage();	
	}

	@Then("^User saves Project$")
	public void user_saves_Project() throws Throwable {
		pages.PlayerPage.saveProject();			
	    pages.SavePage.saveProject();
	    pages.SavePage.closeShareWindow();
	}

	@Then("^User selects video high quality$")
	public void user_selects_video_high_quality() throws Throwable {
	    pages.UploadPage.selectQualityHigh();
	    pages.UploadPage.changeProjectName("High Quality");
	}

	@Then("^New project with selected high quality is displayed on My Projects page$")
	public void new_project_with_selected_high_quality_is_displayed_on_My_Projects_page() throws Throwable {
		pages.MyProjectsPage.checkCreatedProject("High Quality");	    
	}

	@Then("^User selects video low quality$")
	public void user_selects_video_low_quality() throws Throwable {
		pages.UploadPage.selectQualityLow();
	    pages.UploadPage.changeProjectName("Low Quality");	    
	}

	@Then("^New project with selected low quality is displayed on My Projects page$")
	public void new_project_with_selected_low_quality_is_displayed_on_My_Projects_page() throws Throwable {
		pages.MyProjectsPage.checkCreatedProject("Low Quality");
	}

	@Then("^User deletes created project$")
	public void user_deletes_created_project() throws Throwable {
		pages.MyProjectsPage.deleteLatestProject();
	    
	}	
	
	@Then("^User goes to My Videos page$")
	public void user_goes_to_My_Videos_page() throws Throwable {
		pages.MyVideosPage.openMyVideos();	    
	}

	@Then("^User deletes uploaded video$")
	public void user_deletes_uploaded_video() throws Throwable {
		pages.MyVideosPage.deleteLatestVideo();	    
	}
	

}
