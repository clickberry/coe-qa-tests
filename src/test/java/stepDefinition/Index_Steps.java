package stepDefinition;

import cucumber.api.java.en.*;

public class Index_Steps {		
		
	@Given("^User is on Editor Page$")
	public void user_is_on_Editor_Page() throws Throwable {
	    pages.IndexPage.openLoginPage();
	}

	@Then("^Editor Page is correctly displayed$")
	public void editor_Page_is_correctly_displayed() throws Throwable {
	    pages.IndexPage.checkSocialLoginButtons();
	    pages.IndexPage.checkEmailLoginForm();
	}

	@Then("^Screencast is displayed$")
	public void screencast_is_displayed() throws Throwable {
	    pages.IndexPage.checkScreencast();
	}

	@Then("^User closes Screencast$")
	public void user_closes_Screencast() throws Throwable {
	    pages.IndexPage.closeScreencast();
	}	

}
