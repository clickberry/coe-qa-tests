package stepDefinition;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.browser;

import java.io.File;
import java.io.IOException;

import org.junit.After;

import ru.yandex.qatools.allure.annotations.Attachment;

import com.codeborne.selenide.Screenshots;
import com.google.common.io.Files;

import coe.util.PropertyLoader;
import cucumber.api.java.Before;

public class COE_Steps {
	
	@Before    
	  
	public static void setUp() { 		  
		  baseUrl = PropertyLoader.loadProperty("site.url");		  
		  browser = PropertyLoader.loadProperty("browser.name");				  
    }  	
	
	@After
	public void tearDown() throws IOException {
		screenshot();  
	}

	@Attachment(type = "image/png")
	public byte[] screenshot() throws IOException {
	      File screenshot = Screenshots.getLastScreenshot();
	      return Files.toByteArray(screenshot);
	}

}
