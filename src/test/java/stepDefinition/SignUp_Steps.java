package stepDefinition;

import cucumber.api.java.en.*;

public class SignUp_Steps {
	
	@Given("^User goes to Sign Up page$")
	public void user_goes_to_Sign_Up_page() throws Throwable {
	    pages.IndexPage.openSignUpPage();
	}

	@Given("^User enters absent Email value$")
	public void user_enters_absent_Email_value() throws Throwable {
	    pages.SignUpPage.enterSignUpEmail("");
	}

	@Given("^User enters correct Password value$")
	public void user_enters_correct_Password_value() throws Throwable {
	    pages.SignUpPage.enterSignUpPassword("qa123456");
	}

	@Given("^User enters correct Confirm Password value$")
	public void user_enters_correct_Confirm_Password_value() throws Throwable {
	    pages.SignUpPage.enterPasswordConfirm("qa123456");
	}

	@Then("^Sign Up button is disabled$")
	public void sign_Up_button_is_disabled() throws Throwable {
	    pages.SignUpPage.isSignUpDisabled();
	}

	@Then("^Email field is marked as incorrect$")
	public void email_field_is_marked_as_incorrect() throws Throwable {
	    pages.SignUpPage.checkWrongEmail();
	}

	@Then("^User cancels Sign Up$")
	public void user_cancels_Sign_Up() throws Throwable {
	    pages.SignUpPage.goToLoginPage();
	}

	@Given("^User enters short Email value$")
	public void user_enters_short_Email_value() throws Throwable {
	    pages.SignUpPage.enterSignUpEmail("1");
	}

	@Given("^User enters incorrect Email value$")
	public void user_enters_incorrect_Email_value() throws Throwable {
		pages.SignUpPage.enterSignUpEmail("mail.ru");
	}
	
	@Given("^User enters correct Email value$")
	public void user_enters_correct_Email_value() throws Throwable {
		pages.SignUpPage.enterSignUpEmail("correct@mail.ru");
	}

	@Given("^User enters absent Password value$")
	public void user_enters_absent_Password_value() throws Throwable {
		pages.SignUpPage.enterSignUpPassword("");
	}

	@Given("^User enters absent Confirm Password value$")
	public void user_enters_absent_Confirm_Password_value() throws Throwable {
		pages.SignUpPage.enterPasswordConfirm("");
	}

	@Then("^Password field is marked as incorrect$")
	public void password_field_is_marked_as_incorrect() throws Throwable {
	    pages.SignUpPage.checkWrongPassword();
	}

	@Then("^Confirm Password field is marked as incorrect$")
	public void confirm_Password_field_is_marked_as_incorrect() throws Throwable {
	    pages.SignUpPage.checkWrongConfirm();
	}

	@Given("^User enters short Password value$")
	public void user_enters_short_Password_value() throws Throwable {
		pages.SignUpPage.enterSignUpPassword("qa123");
	}

	@Given("^User enters short Confirm Password value$")
	public void user_enters_short_Confirm_Password_value() throws Throwable {
		pages.SignUpPage.enterPasswordConfirm("qa123");
	}

	@Given("^User enters incorrect Confirm Password value$")
	public void user_enters_incorrect_Confirm_Password_value() throws Throwable {
		pages.SignUpPage.enterPasswordConfirm("qa789456");
	}
	
	@Given("^User enters registered Email value$")
	public void user_enters_registered_Email_value() throws Throwable {
	   pages.SignUpPage.enterSignUpEmail("autouser@mail.ru");
	}

	@Then("^User clicks Sign Up button$")
	public void user_clicks_Sign_Up_button() throws Throwable {
	    pages.SignUpPage.clickSignUpSubmit();
	}

	@Then("^Failure message is displayed$")
	public void failure_message_is_displayed() throws Throwable {
	    pages.SignUpPage.checkSignUpFail();
	}
	
	@Then("^Success message is displayed$")
	public void success_message_is_displayed() throws Throwable {
	    pages.SignUpPage.checkSignUpSuccess();
	}

	@Then("^User goes to Portal Profile page$")
	public void user_goes_to_Portal_Profile_page() throws Throwable {
	    pages.MyProjectsPage.checkMyProjects();
	    pages.MyProjectsPage.goToPortal();
	    web.pages.IndexPage.OpenSignIn();
	    web.pages.SignInPage.EmailUserLogin("correct@mail.ru", "qa123456");
	    web.pages.IndexPage.OpenUserMenu();
	    web.pages.IndexPage.OpenSettingsPage();
	}

	@Then("^User deletes created account$")
	public void user_deletes_created_account() throws Throwable {
	    web.pages.SettingsPage.CheckDeletePage();
	    web.pages.SettingsPage.DeleteUserAccount();
	    web.pages.IndexPage.OpenEditor();
	}


}
