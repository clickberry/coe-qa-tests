Feature: Open Editor Index Page
Description: User should be able to open Editor Index Page

@Index 
Scenario: 1.0 Open Editor Index Page
   Given User is on Editor Page
   Then Editor Page is correctly displayed
   And Screencast is displayed
   And User closes Screencast