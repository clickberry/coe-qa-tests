Feature: Login to Editor
Description: User should be able to login to Editor

@SignIn_1 @SignIn
Scenario Outline: 1.1 Successful Login to Editor with Email account
   Given User is on Editor Login Page
   And User enters <correct> Email credentials
   Then User successfully logins 
   And My Projects page is displayed
   And User logouts from editor
   
   Examples:
    | correct | description |
    | autouser@mail.ru | Email user |
    | newusertest@mail.com | Lower case |
    | EmailUserTest@mail.com | Upper case |

@SignIn_2 @SignIn
Scenario: 1.2 Successful login to Editor with Facebook account
   Given User is on Editor Login Page
   And User clicks FB login icon
   And FB window is opened
   And User enters FB credentials
   Then My Projects page is displayed
   And User logouts from editor
   
@SignIn_3 @SignIn
Scenario: 1.3 Successful login to Editor with VK account
   Given User is on Editor Login Page
   And User clicks VK login icon
   And VK window is opened
   And User enters VK credentials
   Then My Projects page is displayed
   And User logouts from editor
   
@SignIn_4 @SignIn
Scenario: 1.4 Successful login to Editor with Twitter account
   Given User is on Editor Login Page
   And User clicks Twitter login icon
   And Twitter window is opened
   And User enters Twitter credentials
   Then My Projects page is displayed
   And User logouts from editor   
