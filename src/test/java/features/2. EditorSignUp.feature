Feature: New user registration in Editor
Description: User should be able to register new account in Editor

@SignUp_1 @SignUp
Scenario Outline: 1.1 Register user with incorrect email
   Given User is on Editor Login Page
   And User goes to Sign Up page
   And User enters <incorrect> Email value
   And User enters correct Password value
   And User enters correct Confirm Password value
   Then Sign Up button is disabled 
   And Email field is marked as incorrect
   And User cancels Sign Up
   
   Examples:
    | incorrect | 
    | absent | 
    | short | 
    | incorrect | 
    
@SignUp_2 @SignUp
Scenario Outline: 1.2 Register user with incorrect password
   Given User is on Editor Login Page
   And User goes to Sign Up page
   And User enters correct Email value
   And User enters <incorrect> Password value
   And User enters <incorrect> Confirm Password value
   Then Sign Up button is disabled 
   And Password field is marked as incorrect
   And Confirm Password field is marked as incorrect
   And User cancels Sign Up
   
   Examples:
    | incorrect | 
    | absent | 
    | short | 
    
@SignUp_3 @SignUp
Scenario Outline: 1.3 Register user with incorrect password confirmation
   Given User is on Editor Login Page
   And User goes to Sign Up page
   And User enters correct Email value
   And User enters correct Password value
   And User enters <incorrect> Confirm Password value
   Then Sign Up button is disabled 
   And Confirm Password field is marked as incorrect
   And User cancels Sign Up
   
   Examples:
    | incorrect | 
    | absent | 
    | incorrect |    
    
@SignUp_4 @SignUp
Scenario: 1.4 Register already existing email
   Given User is on Editor Login Page
   And User goes to Sign Up page
   And User enters registered Email value
   And User enters correct Password value
   And User enters correct Confirm Password value
   Then User clicks Sign Up button
   And Failure message is displayed
   And User cancels Sign Up
   
@SignUp_5
Scenario: 1.5 Register new email account
   Given User is on Editor Login Page
   And User goes to Sign Up page
   And User enters correct Email value
   And User enters correct Password value
   And User enters correct Confirm Password value
   Then User clicks Sign Up button
   And Success message is displayed
   And User goes to Portal Profile page 
   And User deletes created account
   And User returns to Editor Login Page
   
        