Feature: Upload video to Editor
Description: User should be able to upload video file for editing

@Upload_1 @Upload
Scenario Outline: 3.1 Upload video file with selected quality
   Given User is on Editor Login Page
   And User enters Email credentials
   And User successfully logins 
   And My Projects page is displayed
   And User opens Upload Video page
   Then User selects video to upload
   And User selects video <quality>
   And Video is uploading
   And Player page is opened
   And User saves Project
   And New project with selected <quality> is displayed on My Projects page
   And User deletes created project
   And User goes to My Videos page
   And User deletes uploaded video
   And User logouts from editor
   
   Examples:
    | quality | 
    | high quality | 
    | low quality |     
   