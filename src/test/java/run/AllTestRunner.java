package run;
 
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions(
		strict = false,
		features = "src/test/java/features",
		tags = {"@Index, @SignIn, @SignUp, @Upload"},
		glue={"stepDefinition"},
		format = {"pretty", "html:target/cucumber-report", "json:target/cucumber-report/report.json", "junit:target/cucumber-report/junit.xml"}
		)

public class AllTestRunner {
	
 
}


