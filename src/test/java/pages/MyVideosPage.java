package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.sleep;

public class MyVideosPage {
	
    private static SelenideElement myVideosTab = $(By.xpath("//a[@ui-sref='editor.videos']"));
	
	private static ElementsCollection videoMenuButton = $$(By.xpath("//div[@class='menu-icon']"));
	private static ElementsCollection deleteButton = $$(By.xpath("//a[@ng-click='removeVideo(video)']"));
	
	private static SelenideElement dialogWindow = $(By.xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable']"));
	private static SelenideElement confirmDeleteButton = $(By.xpath("//div[@class='ui-dialog-buttonset']/button[1]"));
	
/**
 * Open My Videos page	
 */
	
	@Step("Open My Videos page")
	public static void openMyVideos(){
	
		myVideosTab.click();
		
	}
	
/**
 * Delete latest video	
 */
	
	@Step("Delete latest video")
	public static void deleteLatestVideo(){
		
		videoMenuButton.get(0).click();
		deleteButton.get(0).click();
		sleep(2000);
		dialogWindow.should(exist);
		confirmDeleteButton.click();
		
		
	}

}
