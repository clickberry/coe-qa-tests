package pages;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Condition.*;

import ru.yandex.qatools.allure.annotations.Step;

import org.openqa.selenium.By;


/**
 * Application Login Page. 
 */

public class IndexPage {	
	
	static SelenideElement loginPage = $(By.xpath("//div[@id='login']"));
	
	private static SelenideElement fbButton = $(By.xpath("//a[contains(@href, 'facebook')]"));
	private static SelenideElement vkButton = $(By.xpath("//a[contains(@href, 'vk')]"));
	private static SelenideElement twButton = $(By.xpath("//a[contains(@href, 'twitter')]"));
	
	private static SelenideElement emailField = $(By.xpath("//input[@type='email']"));
	private static SelenideElement emailFieldFail = $(By.xpath("//input[@class='ng-valid-required ng-touched ng-dirty ng-invalid ng-invalid-email']"));
	
	private static SelenideElement passwordField = $(By.xpath("//input[@type='password']"));
	private static SelenideElement signUpLink = $(By.xpath("//a[@ui-sref='user.register']"));
	private static SelenideElement loginButton = $("button.btn.right.ng-binding");
	
	private static SelenideElement message = $("div.toast-message");
	
	private static SelenideElement screencastWindow = $(By.id("screencast-window-dialog"));
	private static SelenideElement screencastClose = $(By.xpath("//button[@class='ui-dialog-titlebar-close']"));
	
	private static SelenideElement logoutButton = $(By.xpath("//a[@ng-click='logout()']"));

/**
 * Open Login Page	
 */

	@Step("Open Login Page")	
	public static void openLoginPage() {	
	
	open(baseUrl);	
	loginPage.waitUntil(visible, 20000);
	sleep(1000);
	
	}	
	
/**
 * Check SN buttons on the Login page	
 */

    @Step("Check SN buttons on the Login page")
    public static void checkSocialLoginButtons() {
    	
    	fbButton.exists();
    	vkButton.exists();
    	twButton.exists();    	
    	
    }  
    
/**
 * Check SN buttons on the Login page    
 */
    
    @Step("Check Email Login form on the Login page")
    public static void checkEmailLoginForm() {
    	
    	emailField.exists();
    	passwordField.exists();
    	signUpLink.exists();
    	loginButton.exists();    	
    	
    }
    
/**
  * Logout from the application	
  */
	@Step("Logout from the application")	
	public static void logout() {			
		
	    sleep(5000);
		logoutButton.click();			
		loginPage.waitUntil(visible, 20000);
			
	}
		    

/**
 * Open Registration Page    
 */    
    
    @Step("Open Sign Up Page")
    public static void openSignUpPage() {
    	
    	signUpLink.click();
    	    	
    }
    
/**
  * Email Account Login 
  */
           
    @Step("Login with Email account")
    public static void emailUserLogin(String Email, String Password) {
           	
        emailField.clear();
        emailField.sendKeys(Email);
        passwordField.clear();
        passwordField.sendKeys(Password); 
        loginButton.click();
           	 	
     }    
    
/**
 * Check Failure message for User
 */
    	
    @Step("Informational message about login failure appears")    
    public static void checkFailMessage() {
    	
    	loginButton.click();
    	message.shouldHave(text("Can't login. Check please your password or email, end try once more!"));
    	message.waitUntil(disappear, 10000);
    		
    }
    	
/**
 * Check Success message for User	
 */
    
    @Step("Informational message about successful login appears")
    public static void checkSuccessMessage() {
    	
    	message.waitUntil(appear, 5000).shouldHave(text("Login successfull"));
    	message.waitUntil(disappear, 10000);
    	
    }    	
    	
/**
 * Check Email Field is incorrectly filled	
 */
    
    @Step("Email field is marked as incorrectly filled")
    public static void checkWrongEmailStatus() {
    	
    	emailFieldFail.is(visible);
    	
    }
    	    	
/**
 * Login in COE via VK  	
 */    	
    
    @Step("Click VK login button")
    public static void loginVK() {
    	
       vkButton.waitUntil(visible, 10000).click();
    		    	
    }
	
/**
 * Login in COE via FB    	
 */
    
    @Step("Click FB login button")
    public static void loginFB() {
    		
    	fbButton.waitUntil(visible, 10000).click();
    		        	
    }
    	
/**
 * Login in COE via Twitter
 */
    	
    @Step("Click Twitter login button")
    public static void loginTwitter() {
    		
    	twButton.waitUntil(visible, 10000).click();	
    	//if (TwitterAuthorize.exists() == true){
    	//TwitterAuthorize.click(); }  
    	//switchToWindow("Clickberry Online Editor");
    		
    }
    
/**
 * Check Screencast window
 */
    
    @Step("Check Screencast is displayed")
    public static void checkScreencast() {
    	
    	screencastWindow.shouldBe(visible);
    }
    
/**
 * Close Screencast window 
 */
    
    @Step("Close Screencast window")
    public static void closeScreencast() {
    	
    	if(screencastClose.exists() == true){
    		screencastClose.click();			
		}
    	
    }
     
	
}
