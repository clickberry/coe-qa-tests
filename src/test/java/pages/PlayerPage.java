package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import ru.yandex.qatools.allure.annotations.Step;

import org.openqa.selenium.By;

/**
 * Application Player Page
 *
 */

public class PlayerPage {
	
	public static SelenideElement player = $(By.id("player"));
	
	private static SelenideElement playButton = $(By.xpath("//button[@id='play_button']"));
	private static SelenideElement pauseButton = $(By.xpath("//button[@id='pause_button']"));
	
	//private static SelenideElement Timeline = $(By.xpath("//div[@id='seekbar']"));
	
	private static SelenideElement muteButton = $(By.xpath("//button[@id='mute']"));
	private static SelenideElement volumeBarMuted = $(By.xpath("//div[@class='volume-slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all ui-state-disabled ui-slider-disabled']"));
	private static SelenideElement volumeBarUnmuted = $(By.xpath("//div[@class='volume-slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all']"));
	
	private static SelenideElement exitButton = $(By.xpath("//button[@ng-click='exit()']"));	
	private static SelenideElement saveButton = $(By.xpath("//button[@ng-click='save()']"));	
	
	private static SelenideElement snapButton = $(By.xpath("//button[@id='snap-button']"));	
	private static SelenideElement tagMenu = $(By.xpath("//span[@class='snap-settings']"));
	
	//private static SelenideElement widgetsMenu = $(By.id("widget-button"));
	private static SelenideElement commentWidgetButton = $(By.xpath("//div[@class='widget menu-widget-comment']"));
	//private static SelenideElement CartWidgetButton = $(By.xpath("//div[@class='widget menu-widget-cart']"));
	
	private static SelenideElement commentWindow = $(By.xpath("//widget-comment-view"));
	private static SelenideElement commentTextArea = $(By.xpath("//widget-comment-edit/div/textarea"));
	private static SelenideElement commentSaveButton = $(By.xpath("//button[@class='glyphicon glyphicon-eye-open']"));
	
	
/**
 * Check Player Page is displayed.	
 */	
	@Step("Check Player Page is displayed")
	public static void checkPlayerPage() {
		
		player.waitUntil(appear, 300000).shouldBe(visible);		
	}
	
/**
 * Play Video. 	
 */
	@Step("Play Video")
	public static void playVideo() {
		
		sleep(2000);
		playButton.click();
		sleep(1000);
		pauseButton.shouldBe(visible);
		
	}
	
/**
 * Pause Video	
 */
	@Step("Pause Video")
    public static void pauseVideo() {
		
		pauseButton.click();
		sleep(1000);
		playButton.shouldBe(visible);		
		
	}
	
/**
 * Mute & unmute Video Sound.	
 */
	@Step("Mute & unmute Video Sound")
	public static void setVideoSound() {
		
		muteButton.click();
		volumeBarMuted.shouldBe(visible);
		muteButton.click();
		volumeBarUnmuted.shouldBe(visible);
	}
	
/**
 * Exit Editor & Return to Upload Video Page	
 */
	@Step("Exit Editor & Return to Upload Video Page")
	public static void exitEditor() {		
		exitButton.click();		
	}
	
/**
 * Add new Snap
 */
	@Step("Add new Snap")
	public static void addSnap() {
		
		snapButton.click();	
		tagMenu.shouldBe(visible);
		
	}
	
/**
 * Add Comment Widget	
 */
	@Step("Add Comment Widget")
	public static void addComment(String CommentText) {
		
		commentWidgetButton.click();
		sleep(10000);
		commentWindow.should(exist);
		commentTextArea.sendKeys(CommentText);		
		commentSaveButton.click();
		
	}
	
/**
 * Save created Project	
 */
	@Step("Save created Project")
	public static void saveProject() {
		
		saveButton.click();
		sleep(2000);
		SavePage.checkSavePage();
		
	}
	

}
