package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

/**
 *  Save page: saving & sharing projects.
 *
 */

public class SavePage {
	
	private static SelenideElement screenshot = $(By.xpath("//div[@class='screenshot-image']"));
	private static SelenideElement selectNextScreenshot = $(By.xpath("//button[@class='screenshot-selector-next']"));
	private static SelenideElement selectPreviousScreenshot = $(By.xpath("//button[@class='screenshot-selector-previous']"));		
	
	private static SelenideElement commentField = $(By.xpath("//div[@class='share-comment']/textarea"));
	private static SelenideElement privacyCheckBox = $(By.xpath("//div[@class='share-type']/label"));	
	
	private static SelenideElement saveButton = $(By.xpath("//div[@id='share-window']/div[@class='share-container']/button[@id='share']"));
	private static SelenideElement cancelButton = $(By.xpath("//button[@id='cancel']"));
	
	private static SelenideElement shareWindow = $(By.xpath("//div[@role='dialog']"));
	private static SelenideElement fbShareButton = $(By.xpath("//span[@class='social icon social-share-facebook']"));
	private static SelenideElement vkShareButton = $(By.xpath("//a[@class='social icon social-share-vk']"));
	private static SelenideElement twitterShareButton = $(By.xpath("//a[@class='social icon social-share-twitter']"));
	private static SelenideElement googleShareButton = $(By.xpath("//a[@class='social icon social-share-google']"));
	private static SelenideElement linkedInShareButton = $(By.xpath("//a[@class='social icon social-share-linkedin']"));
	private static SelenideElement pinterestShareButton = $(By.xpath("//a[@class='social icon social-share-pinterest']"));	
	private static SelenideElement shareCloseButton = $(By.cssSelector("button.ui-dialog-titlebar-close"));

/**
 * Check Save Page is displayed	
 */
	@Step("Check Save Page is displayed")
	public static void checkSavePage() {
		
		screenshot.shouldBe(visible);
		commentField.shouldBe(visible);
		saveButton.shouldBe(visible);
		cancelButton.shouldBe(visible);
		
	}
	
/**
 * Check Screenshot is correctly displayed.	
 */
	@Step("Check Screenshot is correctly displayed")	
	public static void checkScreenshot() {	
		
		screenshot.shouldBe(visible);
		selectNextScreenshot.click();
		screenshot.shouldBe(visible);
		selectPreviousScreenshot.click();
		screenshot.shouldBe(visible);
		
	}
	
/**
 * Create Comment for Project	
 */
	@Step("Create Comment for Project")
	public static void createComment(String ProjectComment) {
		
		commentField.sendKeys(ProjectComment);
	}
	
/**
 * 	Change Privacy to Hidden
 */
	@Step("Change Project Privacy to Hidden")
	public static void setHiddenPrivacy() {
		
		privacyCheckBox.click();
		
	}
	
/**
 * Save Project	
 */
	@Step("Save Project")
	public static void saveProject() {
		
		saveButton.click();
	}
	
/**
 * Share project to Facebook	
 */
	@Step("Click FB Share button")
	public static void fbShare() {
		
		shareWindow.waitUntil(visible, 5000);
		fbShareButton.click();
		
	}
	
/**
  * Share project to Vkontakte	
  */
	@Step("Click VK Share button")
	public static void vkShare() {
			
		shareWindow.waitUntil(visible, 5000);
		vkShareButton.click();
			
	}	
	
/**
  * Share project to Twitter	
  */
	@Step("Click Twitter Share button")		
	public static void twitterShare() {
			
		shareWindow.waitUntil(visible, 5000);
		twitterShareButton.click();
				
	}	

/**
  * Share project to Google
  */
	@Step("Click Google Share button")			
	public static void googleShare() {
			
		shareWindow.waitUntil(visible, 5000);
		googleShareButton.click();
					
	}	

/**
  * Share project to Pinterest
  */
	@Step("Click Pinterest Share button")				
	public static void pinterestShare() {
			
		shareWindow.waitUntil(visible, 5000);
		pinterestShareButton.click();
						
	}	
	
/**
 * Share project to LinkedIn	
 */
	@Step("Click LinkedIn Share button")
	public static void linkedInShare() {
		
		shareWindow.waitUntil(visible, 5000);
		linkedInShareButton.click();
						
	}	
	
/**
 * Close Share window
 */
	
	@Step("Close Share window")
	public static void closeShareWindow() {
		
		shareWindow.waitUntil(visible, 20000);
		shareCloseButton.click();
		
	}
}
