package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.File;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

/**
 *  Upload page: upload from the file by default.
 *
 */

public class UploadPage {
	
	private static SelenideElement uploadVideoButton = $(By.xpath("//a[@ui-sref='editor.upload']"));
	private static SelenideElement uploadVideoTab = $(By.xpath("//a[@class='menu-button upload-video-item selected']"));
	//private static SelenideElement myVideosButton = $(By.xpath("//a[@ui-sref='editor.library']"));
	private static SelenideElement selectVideoButton = $(By.xpath("//button[@class='select-video']"));
	private static SelenideElement fileDownload = $(By.xpath("//input[@type='file']"));
		
	private static SelenideElement projectNameField = $(By.xpath("//input[@name='projectName']"));
	//private static SelenideElement qualitySelectButton = $(By.id("quality_select"));
	private static SelenideElement qualityLowIcon = $(By.xpath("//span[@class='switch-right ng-binding']"));
	private static SelenideElement qualityHighIcon = $(By.xpath("//span[@class='switch-left ng-binding']"));
	
	private static SelenideElement uploadButton = $(By.xpath("//button[@class='btn-upload right ng-scope']"));		
	private static SelenideElement uploadProgress = $(By.xpath("//div[contains(@ng-class, 'ENCODING_VIDEO')]"));
	private static SelenideElement uploadFail = $(By.xpath("//div[@class='toast-message']"));
	
/**
  * Open Upload Video Page	
  */
	@Step("Open Upload Video Page")
	public static void openUploadVideoPage() {
	
	sleep(5000);
	uploadVideoButton.click();
	}	
	
/**
 * Check Upload Page is correctly displayed.	
 */
	@Step("Check Upload Page is correctly displayed")
	public static void checkUploadVideoPage() {
		
		uploadVideoTab.should(exist);
		selectVideoButton.should(exist);
	}	
	
/**
 * Open My Videos page
 * 
 */
 /*
	@Step("Open My Videos page")
	public static MyVideos UploadMyVideos() {
		
		MyVideosButton.click();
		return page(MyVideos.class);
		
	}
*/	

	
/**
 * Select video file	
 */
	@Step("Select video file")
	public static void selectVideoFile() {
		
		String script = "arguments[0].style.display='block';"			    
			          + "return true;";
		
		executeJavaScript(script, fileDownload);
		
		File NewFile = new File("C:\\Users\\Public\\Videos\\Sample Videos\\Wildlife.wmv");
		
		fileDownload.uploadFile(NewFile);		
		
		sleep(5000);
	}
	
/**
 * Change Project Name	
 */
	@Step("Change Project Name")
	public static void changeProjectName(String ProjectName) {
		
		projectNameField.clear();
		projectNameField.sendKeys(ProjectName);
			
	}
	
/**
 * Select video quality = High	
 */
	@Step("Select video quality = High")
	public static void selectQualityHigh() {
		
		qualityLowIcon.click();
		qualityHighIcon.shouldBe(visible);	
		
	}
	
/**
  * Select video quality = Low	
  */
	@Step("Select video quality = Low")	
	public static void selectQualityLow() {

		qualityLowIcon.shouldBe(visible);
		qualityLowIcon.click();
		qualityHighIcon.click();
		qualityLowIcon.shouldBe(visible);		
			
	}
	
/**
 * 	UploadFile - Successful
 */
	@Step("File successfully uploaded")
	public static void uploadFilePass() {
		
		uploadButton.click();
		uploadProgress.waitUntil(visible, 50000);		
		
	}
	
/**
 * 	UploadFile - Fail
 */
	@Step("File upload fails")	
	public static void uploadFileFail() {
			
		uploadButton.click();
		uploadFail.waitUntil(visible, 2000).shouldHave(text("Can't create project"));	
		uploadProgress.waitUntil(disappear, 10000);	
			
	}	

}
