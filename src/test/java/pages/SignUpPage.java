package pages;

import org.openqa.selenium.*;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.disabled;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import ru.yandex.qatools.allure.annotations.Step;

/**
 * Application Registration Page. 
 */

public class SignUpPage {
	
	private static SelenideElement loginLink = $(By.xpath("//a[@ui-sref='user.login']"));
		
	private static SelenideElement emailField = $(By.xpath("//input[@name='email']"));
	private static SelenideElement emailFieldFailIcon = $(By.xpath("//span[@ng-show='register.email.$invalid']"));
		
	private static SelenideElement passwordField = $(By.xpath("//input[@name='password']"));
	private static SelenideElement passwordFieldFailIcon = $(By.xpath("//span[@ng-show='register.password.$invalid']"));
	
	private static SelenideElement confirmPasswordField = $(By.xpath("//input[@name='password2']"));
	private static SelenideElement confirmFieldFailIcon = $(By.xpath("//span[@ng-show='register.password2.$invalid']"));
	
	private static SelenideElement signUpButton	= $(By.xpath("//button[@class='btn btn-info right ng-binding']"));
	
	private static SelenideElement message = $("div.toast-message");
	
	
/**
 * Open Login Page	
 */
	@Step("Return to Login Page")
	public static void goToLoginPage() {		
		
		loginLink.click();
				
	}
	
/**
 * Enter user email. 
 * @param Email
 */
	@Step("Enter user email")
	public static void enterSignUpEmail(String Email) {		

		emailField.clear();
		emailField.sendKeys(Email);			
	}
	
/**
 * Enter user password	
 * @param Password
 */
	
	@Step("Enter user password")
	public static void enterSignUpPassword(String Password) {
		passwordField.clear();
		passwordField.sendKeys(Password);
	}
	
/**
 * Enter password confirmation	
 * @param ConfirmPassword
 */
	
	@Step ("Cornfirm entered password")
	public static void enterPasswordConfirm(String ConfirmPassword) {
		confirmPasswordField.clear();
		confirmPasswordField.sendKeys(ConfirmPassword);	
	}
	
/**
 * Click Sign Up button	
 */
	
	@Step("Click Sign Up button")
	public static void clickSignUpSubmit() {
		
		signUpButton.click();
		
	}
	
/**
 * Sign Up button is disabled	
 */
	
	@Step("Sign Up button is disabled")
	public static void isSignUpDisabled() {
		
		signUpButton.shouldBe(disabled); 
		
	}
	
/**
 * Successful registration	
 */
	@Step("Sign up success message is displayed")
	public static void checkSignUpSuccess() {
		
		message.shouldHave(text("Welcome to Clickberry online editor"));
		message.waitUntil(disappear, 10000);
	}
	
/**
 * User Sign Up Failure
 */
	@Step("Sign up failure message is displayed")	
	public static void checkSignUpFail() {	
		
		message.shouldHave(text("Can't register. Please check your email or username, end try once more!"));
		message.waitUntil(disappear, 10000);	
			
	}
	
/**
 * Check Email Field is incorrectly filled	
 */
	@Step("Check Email Field is incorrectly filled")
	public static void checkWrongEmail() {
		
		emailFieldFailIcon.is(visible);
	}	
 
/**
  * Check Password Field is incorrectly filled	
  */
    @Step("Check Password Field is incorrectly filled")
	public static void checkWrongPassword() {
    	
		passwordFieldFailIcon.is(visible);
		
	}
    
/**
  * Check Password Confirmation Field is incorrectly filled	
  */
    @Step("Check Confirm Password Field is incorrectly filled")
   	public static void checkWrongConfirm() {
       	
   		confirmFieldFailIcon.is(visible);
   		
   	}	    
	
}
