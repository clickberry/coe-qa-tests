package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class MyProjectsPage {
	
	private static SelenideElement clickberryLogo = $(By.xpath("//div[@id='menu']//a[contains(@href, 'clickberry')]"));
	private static SelenideElement myProjectsTab = $(By.xpath("//a[@class='menu-button my-videos-item selected ng-binding']"));
	
	private static ElementsCollection projectInList = $$(By.xpath("//div[@id='library']//div[@class='my-projects-project ng-binding ng-scope']"));
	private static ElementsCollection projectMenuButton = $$(By.xpath("//div[@class='menu-icon']"));
	private static ElementsCollection deleteButton = $$(By.xpath("//a[@ng-click='removeProject(project)']"));
	
	
	
/**
  * Go to Clickberry Portal.    
  */
	    
    @Step("Go to Clickberry Portal")
    public static void goToPortal() {
       sleep(5000);
	   clickberryLogo.click();
	   sleep(5000);	 
	   
	}
    
/**
 * Check My Projects Page.    
 */
    
    @Step("Check My Projects Page")
    public static void checkMyProjects() {
    	refresh();
    	myProjectsTab.should(exist);
    }
    
/**
 * Check created project on My Projects tab    
 */
    
    @Step("Check created project on My Projects tab")
    public static void checkCreatedProject(String ProjectName){

		refresh();
		projectInList.get(0).waitUntil(visible, 20000);
    	projectInList.get(0).shouldHave(text(ProjectName));
    	
    }
    
    
/**
 * Delete created project    
 */
    
    @Step("Delete latest project")
    public static void deleteLatestProject(){
    	
    	projectMenuButton.get(0).click();
    	deleteButton.get(0).click();
    	
    	
    	
    }

}
